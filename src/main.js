import Vue from 'vue'
import App from './App.vue'

// Bulma and Buefy - Theme
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
Vue.use(Buefy)

// Local styles and colors
require('./assets/sass/main.scss')

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
