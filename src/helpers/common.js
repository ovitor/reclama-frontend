import axios from 'axios';

var url = ''
if (process.env.NODE_ENV === 'production') {
  url = 'http://localhost/api/'
} else {
  url = 'http://localhost:8000/api/'
}

export const http = axios.create({
  baseURL: url, 
})
